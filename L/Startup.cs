﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(L.Startup))]
namespace L
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

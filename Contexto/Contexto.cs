﻿using System.Data.Common;
using System.Data.Entity;

namespace Contexto
{
    public class Contexto : DbContext
    {
        public Contexto()
            : base("Contexto")
        {
        }

        public Contexto(DbConnection DbConexao, bool Desconectar = true)
            : base(DbConexao, Desconectar)
        {
        }

        public System.DateTime DataServidor()
        {
            var resultado = Database.SqlQuery(typeof(string), "select now()");
            foreach (var item in resultado)
                return System.Convert.ToDateTime(item.ToString());
            return System.DateTime.Now;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Contexto
{
    [Table("usuario")]
    public class Usuario
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public int Contribuidor { get; set; }
        public string Img { get; set; }
        
    }
}

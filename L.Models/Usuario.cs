﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace LModelo
{
    [Table("usuario")]
    public class Usuario
    {
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Display(Name = "Login")]
        public string Login { get; set; }
        [Display(Name = "Senha")]
        public string Senha { get; set; }
        [Display(Name = "Contribuidor")]
        public int Contribuidor { get; set; }
        [Display(Name = "Foto de perfil")]
        public string Img { get; set; }
    }
}

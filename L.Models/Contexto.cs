﻿using System;
using System.Data.Common;
using System.Data.Entity;

namespace LModelo
{
    public class Contexto : DbContext
    {
        public Contexto()
            : base("Contexto")
        {
        }

        public Contexto(DbConnection DbConexao, bool Desconectar = true)
            : base(DbConexao, Desconectar)
        {
        }

        public DateTime DataServidor()
        {
            var resultado = Database.SqlQuery(typeof(string), "select now()");
            foreach (var item in resultado)
                return Convert.ToDateTime(item.ToString());
            return DateTime.Now;
        }
    }

}
